---
title: "Matrix identification using matrix math"
date: 2020-04-01T21:30:34-06:00
tags:
 - petrophysics
---

### Matrix identification 

Matrix identification (MID) refers to a lithology solution in petrophysics where the bulk density and photoelectric factor are used to estimate the lithologies of the rocks being logged.

When a standard triple combo log is run in a well, the tool is utterly naive to the true identity of the rocks it encounters. The bulk density for example really measures electron density, and the neutron log (grossly simplified) measures hydrogen, and neither has any clue about what lithology it is looking at. 

Instead it says, "this soup of solids and fluids has these electrochemical and nuclear properties." It doesn't say which specific combination of minerals and fluids are present, and indeed any number of combinations would result in the same raw readings. It is up to the petrophysicist to interpret the readings.

MID is one way to guess at a simple lithology. The basic idea is this: take bulk density and subtract porosity to get the _apparent matrix density_ (ρ<sub>maa</sub>), and take the photoelectric factor and manipulate it to get the (porosity-free) _apparent volumetric photoelectric cross-section_ (U<sub>maa</sub>). Then you cross-plot those two parameters and see where the points plot on a ternary diagram, defined by ideal mineral endmembers (plotted as red dots and labeled):

![Umaa-Rhomaa or matrix identification/MID plot](/post/images/umaa-rhomaa.png)

### Matrix math

How do we go from a ternary plot like the one above to a quantitative estimate of lithology? One way is to use matrix math to identify the, _ahem_, matrix. In other words, we use linear algebra to estimate mineral volumes. 

After we've subtracted out porosity to get ρ<sub>maa</sub> and U<sub>maa</sub>, we can solve for three minerals. But I first should introduce the unity equation. It's an assumption we have to make to get the math to work. All the unity equation says is that the volumes of these three minerals sum to make up the entirety of the rock matrix. Traditionally the three minerals identified with MID are dolomite, calcite, and quartz, so I'll use those in this example.

<code>
1 = V<sub>dol</sub> + V<sub>cal</sub> + V<sub>qtz</sub>
</code>

With that assumption, we can then say that the values we have from logs for ρ<sub>maa</sub> and U<sub>maa</sub> are a mix of the ρ<sub>maa</sub> and U<sub>maa</sub> values of each mineral, proportional to their respective volumes. That's a mouthful-- maybe the equations make it clearer:

<code>
ρ<sub>maa</sub> = (ρ<sub>dol</sub> ∙ V<sub>dol</sub>) + (ρ<sub>cal</sub> ∙ V<sub>cal</sub>) + (ρ<sub>qtz</sub> ∙ V<sub>qtz</sub>)<br>
U<sub>maa</sub> = (U<sub>dol</sub> ∙ V<sub>dol</sub>) + (U<sub>cal</sub> ∙ V<sub>cal</sub>) + (U<sub>qtz</sub> ∙ V<sub>qtz</sub>)
</code>

We know the ρ and U of each of these components from chartbooks (reference books provided by logging vendors), so just three unknowns remain: the volumes of dolomite, calcite, and quartz. These three equations (to go with the three unknowns) can be reframed as a linear algebra expression:

<code>
C ∙ V = L
</code>

where L is a _vector_ of the log responses (measured by logging tools), C is a _matrix_ of component petrophysical properties (i.e. ρ<sub>maa</sub> and U<sub>maa</sub> from chartbooks), V is a _vector_ of proportions of each component (unknown). Densities of quartz, calcite, and dolomite respectively are 2.65, 2.71, and 2.87 g/cc, and the U values are 4.8, 13.8, and 9.0 b/e respectively, so here's what it looks like when we plug in those values:

![C times V equals L](/post/images/cvl.png)

To solve for the vector V, we rearrange the equation by multiplying both sides of the equation by the inverse of C (that is, C<sup>-1</sup>) to get:

<code>
V = C<sup>-1</sup> ∙ L
</code>

When we invert C and do the algebra, the equation looks like this:

![V equals inverse of C times L](/post/images/vinvcl.png)

This expression can be rewritten as discrete equations to get the volumes for each mineral component:

<code>
V<sub>qtz</sub> = −0.641 ∙ ρ<sub>maa</sub> − 2.106 ∙ U<sub>maa</sub> + 2.747<br>
V<sub>cal</sub> = −0.106 ∙ ρ<sub>maa</sub> + 0.125 ∙ U<sub>maa</sub> - 0.018<br>
V<sub>dol</sub> = 3.211 ∙ ρ<sub>maa</sub> + 4.980 ∙ U<sub>maa</sub> - 7.192<br>
</code>

In some cases this may result in negative volumes or volumes greater than 100% (when points fall outside the ternary plot). So what I typically do is clip results to 0 and 1 and re-standardize to 100%.

That is the basic methodology. For a more in-depth treatment of the topic, don't miss John Doveton's _Geologic Log Analysis Using Computer Methods_ (1994). In a future post, I'll use some real well log data and make some plots using python, so check back!
