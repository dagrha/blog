---
title: "Get one long tiff from a multipage-PDF with these two simple commands"
date: 2020-07-08T19:40:00-00:00
tags:
 - imagemagick
 - petra
 - "command line tools"
---

Every once in a while I receive a well log or mud log from a client in multipage-pdf form. In other words, rather than a single, very tall raster image (in tiff format, for example), the image has been cut up into many, say, 8.5x11-inch or A4 pages and saved in pdf format. Instead, I often want a file as a single, long page in tiff format for use in Petra or to send off to a digitizer. 

Here's how I use the wonderful command line program [ImageMagick](http://www.imagemagick.org/) to transform a multipage-pdf into single page tiff. It's a two step process.

First, after ImageMagick is installed, `cd` to the directory where the PDF is and issue this command at the terminal:

```
convert -density 300 MULTIPAGE_PDF.pdf -alpha remove -compress lzw page_%0d.tiff
```

That command will open the pdf and spit out each page to its own tiff file. Here is what each argument does.

 - The image resolution can be controlled with the `-density` flag, here specified as 300 dpi. The larger the number, the larger the tiff files will be and the longer the command will take to run. 
 - `-alpha` with the `remove` keyword will remove the transparency of the pdf; in some cases it may be necessary to also explicitly specify a background color for the tiff files, for example `-background white`. 
 - `-compress` with the `lzw` keyword will compress the tiffs. 
 - Last is the pattern for the name of the output files. The `%0d` nugget will return two-digit page numbers (e.g. 01, 02, ... 98, 99). This ensures that the page order is preserved when the pages are recombined.

The second step is to combine all the tiffs into a single page with this command:

```
convert -append page_*.tiff SINGLEPAGE_TIFF.tiff
```

The multipage-pdf has now been transformed into a single-page tiff. Image magic indeed!