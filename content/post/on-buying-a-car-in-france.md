---
title: "On buying a car in France"
date: 2019-07-12T04:36:55-06:00
toc: true
tags: 
 - travel
 - france
---

_What follows is a mostly unedited re-submission of a post I made in 2006 on the now-defunct Assistants-in-France forum. Indeed that year I went through the ordeal of buying a car in France. At the time, as far as I could tell there was no guide like this already out there. By now there may very well be dozens of walkthroughs of the process on the internet. Some of the information in this post is useful, and some of it quaint ("calling cards"!). In any case I wanted to re-post my original write-up, since the forum is long gone, in case others are looking to explore France the way I did--_ par voiture!

___

I came to France knowing that I wanted to have a car. Clermont-Ferrand is a very central city, and I thought a car would allow me to see a lot of things that are not accessible by train or by plane. This is as much a guide as it is a record of my experiences in trying to buy an older used car in France. 

## Driver's license

My U.S. driver's license is valid in France for one year, so long as it comes with a translation. I took care of this before I came to France with a company in Minneapolis. They translated my birth certificate, also, for a competitive rate. On the other hand, there are certainly notarized translators in France. Does the translation have to be notorized? Probably not. In all the _démarches administratives_ for buying a car, I found that no one cared if I had a driver's license, or at least I was never asked to present it. However, the day I get pulled over by the police, I want to make sure that all my paperwork is in order and that I have a translation ready. I am not current on the law concerning this, but I believe a notorized translation is obligatory. Like I said, though, the authorities seemed more interested in being able to read my documents (birth certificate, e.g.) than it being a notorized copy. Better safe than sorry. 
 
If I were staying for more than one year, I would need to get an international driver's license, which means taking the driver's test again. There may also be a way to just get a license for France. On the other hand, France has an agreement with several states in the U.S. making driver's licenses reciprocally valid. Unfortunately, Minnesota is not one of these states. At the end of one year, a driver's test in France would be necessary.

## Finding a suitable car

France has a lot of great little cars. For someone with my budget, the best options are the "great little cars" from the 80s and early 90s. I was in France for two months before buying a car, not because there was an overwhelming amount of paperwork, but because searching for a car that suits your budget and your personal taste/intention takes time. The first month I spent getting to know French cars a bit by look and by make. The carmakers here are often not found at all in the US. For my budget and my intentions, I found a couple models that would have worked from several different carmakers: 
 
*Carmaker* | *Model* | *Comments* 
---|---|--- 
Citroën | AX | Small car. Pretty ancient in general. Mostly found with 3 doors, but some with 5. Occasionally these cars are diesels. 
Peugeot | 205 | These _deux-cent cinqs_ are old cars that run forever (according to a Frenchman). Both diesel and gasoline motors are availible. 
Peugeot | 106 | Like the 205 but a little newer and generally more expensive. 
Renault | Clio | This car is great if you can afford one. They're highly sought after in France, especially the diesels, because they are very reliable. For that, you may have to pay a little more. 
Renault | 5 | The Renault 5 (aka _Super 5_, _Cinq_, Five, etc.) is a neat little car that is from the same generation as the Peugeot 205. Diesel and gasoline engine models exist. There's even a Renault 4 that's even more ancient that sometimes runs on leaded gas. 
Opel | Corsa | A german car. Made by GM. You've been warned. They probably run okay, though. The model I drove had a really worn clutch which made it tough to drive. 
Volkswagen | Golf, Polo | We have Golfs in the U.S. The polo is a little different, but pretty much the same as far as I can tell. 
 

### Transmissions

Most older cars in France have manual transmissions. Few automatics exist, and they are more expensive. That may be changing now, but it is still certainly true with the late-80s/early-90s vehicles. So get your old-fashioned mom or dad to teach you to drive a stick, you lazy bum. 

### Fuels

The compact diesel engine is much more common in France than it is in the US. Whereas we find diesel engines in F350s and buses and an occasional TDi at home, a diesel motor is commonplace in France. From what I've been told, diesel engines not only get better gas mileage, they also need less maintainence (apparently). Diesel fuel is cheaper than gasoline, to boot. Consequently, diesels tend to have good resale values here in France, so be prepared to pay a pretty penny for a car with a diesel engine. One drawback-- diesel engines burn dirtier than gasoline, hippie. 
 
In France, the words _gasoil_ and _diesel_ refer to diesel, while _essence_ and _sans plomb_ refer to gasoline. So, don't fill your gasoline engine up with _gasoil_! The price right now of diesel is about 1€/liter while gasoline is about 1.15€/liter. With about 3.8 liters in a gallon, we're talking real money here: $3.80 per gallon of diesel and almost $4.40 per gallon of _sans plomb_(unleaded). They've been paying these prices for years, too. 

### Where to find this car

In Clermont, two newspapers dominate the classifieds scene: Info and ParuVendu. Info is something of a _bordel_, lacking any real organization. ParuVendu is much easier to browse. I think La Montagne, the local daily publication, also has a classifieds section. You'll be making a lot of phone calls, so get yourself a good calling card that you can use from the phone booth. My school was nice enough to let me use their phone a lot of the time, which minimized costs. In any case, you probably won't want to buy the first car you call about, because 1) you've had nothing to compare it to and 2) no one's that lucky. There are also a number of websites you can visit, including jannonce.fr and ebay.fr. There you can get an idea of prices. In addition, there is a publication which supposedly exists called L'Argus that lists prices of automobiles. It could be a good place to start. 
 
In reality, you can buy a car from anywhere in France without too much hassle. However, it is easiest to purchase a car in the _départment_ in which you live. For example, I live in Puy-de-Dome which is number 63. In order to be the owner of a car, my license plates must be registered in the _départment_ in which I live. That is to say that if I lived in Puy-de-Dome and bought a car from Paris (19, say), it would be necessary to order new license plates (about 20€ and some more paperwork, from what I understand) from the _département_'s _préfecture_. An easy way to verify this is to look at the license plate's _N° d'immatriculation_ or license plate number; the last two numbers listed always refer to the _département_ in which the vehicle is registered. If the plates are 63 and you live in 63, you won't need to change the _plaques_. On the other hand, if the plates say 05 and you live in 63, you'll have to get them changed. Or, you may just want to get the hell out of Auvergne and move to 05 (Nice, that is!). 
 

## The _Démarches Administratives_

When you do find that dreamy Peugeot or sexy Renault, here's a list of things you'll need to make it yours: 

 * _La carte grise_
 * _Certificat de non-gage_
 * _Certificat de contrôle technique_
 * _Certificat de cessation_
 * _Justificat de domicile_
 * _Pièce d'identité_

### _La Carte Grise_

This is basically the title of the car. The _carte grise_ is in the name of the owner. When you're buying a car here, make sure that the _carte grise_ is in the name of the person who's selling it to you, or, at the very least, make sure there's nothing fishy going on, or you could end up buying a stolen car. It's been my experience that people generally just want to get rid of these old cars, and they're often willing to take advantage of the naive foreigner to make a buck. So make sure the _carte grise_ is in order. If the _vendeur_ doesn't have his _carte grise_, a red flag should immediately go up in your head: whenever driving the car, the carte grise must, by law, be with the driver. Unless he's got a good excuse, I'd abandon ship and swim elsewhere. 
 
When you purchase the car, you'll need to get a new _carte grise_, one in your name. That process is described below. 

### _Certificat de non-gage_

I don't really know what this thing is, but I think you need to make sure it's in order. It's also called the _certificat de situation_. I don't think the friendly lady at the _préfecture_ even looked at mine, but I had it anyways. This certificate basically tells the person buying the car whether there are any outstanding legal or financial problems. That is, are there unpaid parking tickets? Has this car been used as collateral on a credit card? Are there dead bodies in the trunk? You can obtain this certificate free-of-charge at your _département_'s _préfecture_, if the seller doesn't already have one for you. If he doesn't it would be a good idea to obtain one and check it out before you buy the car. To get one, you'll need the license plate number (_N° immatriculation_) and the date the car was first used (_Date de 1ère immatriculation_). 
 

### _Certificat de contrôle technique_

This is French socialism at its best. Any car older than 5 years or so must pass a _contrôle technique_, or a mechanical test, every 2 years. To pass the test the car must run safely and must not have any major defaults. That is to say, rust on the body of the car will probably not be enough to flunk the test, but not having brakes would make the car fail the test. Just because a car passes the test doesn't mean it's perfect, by any means. 
 
When someone wants to sell a car, his car must have passed the _contrôle technique_ within the last 6 months. So right there, the purchaser has some protection against a buying a complete lunker. It's a nice guarantee, but it's not perfect. Just remember not to be too hard on your car, either; if you're planning on selling it at the end of your _séjour_, you'll have to have it pass the _contrôle_ also! When going to look at a car, bring a french-english dictionary so that you can decipher the _procès-verbal de contrôle technique_, which can have a lot of strange looking words that the average FFL student would not know. Make sure that you understand what's wrong with the car. There will be two sections: one listing faults that must be fixed with a _contre-visite_ and another listing the car's faults that need not be repaired immediately. 
 
As for bargaining, you may be able to bargain down the price of a car by mentioning that the _contrôle_ is almost expired or something like that. A _contrôle_ is worth about 60€, just so you know. 

### _Cerificat de cession_

It is the responsibility of the seller of the vehicle to print out three of these forms from the _Préfecture_ website. When he fills it out and signs it, he is basically just agreeing that he is selling the car and no longer claiming ownership of the vehicle. This should go hand-in-hand with the old _carte grise_; you'll need both to get a new _carte grise_. This should be filled out three times: you keep one and the seller keeps two. 
 

### _Justificat de Domicile_

A justification of your address is necessary for obtaining the new _carte grise_. An EDF bill or a copy of your lease should suffice. If you don't live in the _départment_, I'm not sure you'll be able to get license plates for the car, which means you won't be able to own the car. There may be a way around this, but if there is, I don't know about it. 
 

### _Pièce d'identité_

It is important to remember that a Passport is not always considered a valid form of identity in France. In order to purchase a car legally, you'll need your _carte de séjour_ (residence permit) or at least the _récipissé_ (receipt of application). The _récipissé_ worked fine for me. 

### Getting your new _carte grise_

Once you have all these things in order, you can go to the _préfecture_ to apply for the new _carte grise_. It costs 30€ per _"cheval"_, 15€ if the car is older than ten years. Most of the older Peugeot 205s, for example, are _quatre chevaux_, or "4 horses," so it'll run you about 60€ in all to obtain the _carte grise_. Bring all this paperwork with you. When I applied I was in and out of there in about 45 minutes, but I was also second in line. Be prepared to wait a while at the _préfecture_ if you live in a big city. 

## Insurance 

Car insurance (_assurance automobile_) is obligatory in France. Often your bank offers an insurance. I went through Crédit Agricole and it costs about 24€ per month with my credentials, the type of car I bought, and the youth discount. When you get insurance, you'll be given a small pocket to attach to the windshield of your vehicule into which you must put a current proof of insurance. This is required, and police officers even do check for this occasionally. It wasn't difficult to obtain insurance because I've been insured for 6 or 7 years in the US. If you've never been insured before, it may be more expensive/more difficult to insure yourself.

___

That's it! That was the post from the Assitants-in-France forum. I also had a follow up post detailing the car I ended up purchasing but I was unable to recover it. 

In the end I purchased a 1990 Peugeot 205, specifically a relatively rare _Champion_ model. Only about 3 or 4 thousand of this three-door hatchback were made and only for the one year. It was a fantastic car for my purposes and I would buy another 205 in an instant if I got to do it over. Here is a shot of the car on the day I purchased it. I had taken a bus out to a relatively remote part of the suburbs to look at the vehicle, and at least in part I bought the vehicle so I wouldn't have to wait for the bus to get home!

![1990 Peugeot 205 Champion the day I purchased it](/post/images/peugeot_205_champion.jpg)

Here's another shot of the vehicle on one of my adventures, delivering my friend Shaun and me to Château d'Alleuze in Auvergne.

![Peugot 205 Champion and Chateau d'Alleuze](/post/images/peugeot_205_champion_near_chateau_d'alleuze.jpg)
