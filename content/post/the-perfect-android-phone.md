---
title: "The Perfect Android Phone"
date: 2019-09-14T09:14:09-06:00
tags:
 - android
 - lineageos
---

I'd put up with my old trusty Nexus 4 for long enough. The coup de grâce was a drop to the basement floor which left half the screen unresponsive. So I started searching for a replacement.

My requirements were pretty basic. I hoped to find a phone that was officially supported by [LineageOS](https://lineageos.org/) (a stable and fantastic fork of AOSP). The Lineage version I had been running on my Nexus 4 (namely 14.1) was no longer receiving security updates, so I thought it was a good time to upgrade to a maintained kernel. I also liked the relatively diminutive size of the Nexus 4. Most of the phones out there these days are enormous, unwieldy, and require two hands to operate, and I wanted to quickly screen official Lineage phones for size.

As part of my effort to find the perfect phone, I used the excellent [Scrapy](http://docs.scrapy.org/en/latest/) library to scrape the [Lineage wiki Github repo](https://github.com/LineageOS/lineage_wiki) for information about the phones, including phone dimensions, currently supported versions, and whether the phone is actively maintained. ([Stay tuned](https://hallau.world/index.xml) for a post or two about using Scrapy!)

I dropped the scraped data into the interactive DataTables table below (also available directly [here](https://hallau.world/phones.html)). You'll notice that the data in the table are not 100% complete. Indeed the Lineage wiki, while extremely useful and informative, is not totally comprehensive yet. I will work on adding the missing data to the wiki, so check back to this page for more complete device information. 

Sorting the table by phone height, you can see that there are virtually no recent compact phones (for which the wiki had device size data, anyways) that are actively maintained official Lineage devices (if there are names listed in the `maintainers` column, that means the phone is maintained, and the versions 15.1 and 16.0 are the officially maintained versions of Lineage at present). 

The smallest phone with a Lineage version >= 15.1 is the Sony Xperia V, a device released in 2012 (!). Indeed several of the Xperia line are very small (around 5 inches) but are either unsupported officially or long in the tooth. The Wingtech Redmi 2, released in 2015, is about the same size as the Nexus 4 and also has an official Lineage version 15.1 release. That phone sells new for around $100 on [ebay](https://www.ebay.com/sch/i.html?_nkw=redmi+2). 

<iframe src="/phones.html" width="100%" height="800px" frameborder="0"></iframe> 

The diminutive size of the Xperia line had me intrigued. After some research I found the Sony Xperia Z1 Compact and Z3 Compact (released 2014, so still pretty old) come in at 5 inches. Neither will be found in the table above as they are not officially supported by Lineage. However, there are unofficial Lineage builds of versions 15.1 and 16.0 available on [XDA-Developers forum](https://forum.xda-developers.com/sony-xperia-z1-compact/development). At less than $90 on ebay, the Sony Xperia Z1 Compact is a capable and small form factor phone at a reasonable price. There is also a Z5 Compact that was released in 2015, but there are no current Lineage builds on XDA, unfortunately.

So, a recent phone around 5 inches or less that is official supported by Lineage does not exist. However, with a couple compromises, there are a few adequate phones at an attractive price point. There's no _perfect_ Android phone at the moment, but there are a couple that are _perfectly fine_.