---
title: "Contact"
exclude_from_rss: true
---

<form name="contact" method="POST" netlify-honeypot="bot-field" data-netlify="true">
  <p class="hidden">
    <label>Don’t fill this out if you're human: <input name="bot-field" /></label>
  </p>
  <p>
    <label>Name: </label>
    <input type="text" name="name" required=""/>
  </p>
  <p>
    <label>Email: </label>
    <input type="text" name="email" required=""/>
  </p>
  <p>
    <label>Message: </label>
    <textarea name="message" required=""></textarea>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>