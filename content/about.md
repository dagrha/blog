---
title: "About"
date: 2019-07-09T12:07:24-06:00
draft: false
exclude_from_rss: true
---

<img class="portrait" src="/img/spain.png">

My name is Dan. I am a geologist living and working in Denver, Colorado, USA. I love to write python at work and in my spare time. 

As a consultant, I develop petrophysical models and perform geologic interpretations, build robust and reliable web scrapers, and do vitrinite reflectance microscopy. 

[I'd love to hear from you.](./contact)

